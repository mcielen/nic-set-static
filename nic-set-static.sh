#!/bin/bash

########################################
# global vars
########################################
nic=""
########################################
# function definitions
########################################
is_valid_ipv4() {
  local -a octets=( ${1//\./ } )
  local RETURNVALUE=0

  # return an error if the IP doesn't have exactly 4 octets
  [[ ${#octets[@]} -ne 4 ]] && return 2

  for octet in ${octets[@]};do
    if [[ ${octet} =~ ^[0-9]{1,3}$ ]];then # shift number by 8 bits, anything larger than 255 will be > 0
      ((RETURNVALUE += octet>>8 ))
    else # octet wasn't numeric, return error
      return 1
    fi
  done
  return ${RETURNVALUE}
  # testtest
}

create_file() {
        cat << EOF >> $file
DEVICE="$nic"
BOOTPROTO=static
EOF
     ipaddrformatcorrect="false"
     curipaddr=$(nmcli dev show eth0 | grep IP4.ADDRESS | cut -d " " -f 26 | cut -d"/" -f 1)
     is_valid_ipv4 $curipaddr
     if [[ $? -gt 0 ]]; then
         curipaddr=""
     fi
     while [ "$ipaddrformatcorrect" != "true" ]; do
        echo -n "Ip address? [$curipaddr]"
        read ipaddress
        is_valid_ipv4 $ipaddress
        if [[ $ipaddress = "" ]]; then
            ipaddress=$curipaddr
        fi
        if [[ $? -gt 0 ]]; then
            ipaddrformatcorrect="false"
        else
            ipaddrformatcorrect="true"
        fi
     done
# curipaddr=$(nmcli dev show eth0 | grep IP4.ADDRESS | cut -d " " -f 26 | cut -d"/" -f 1)
#curprefix=$(nmcli dev show eth0 | grep IP4.ADDRESS | cut -d " " -f 26 | cut -d"/" -f 2)
cat << EOF >> $file
IPADDR=$ipaddress
EOF
    curprefix=$(nmcli dev show eth0 | grep IP4.ADDRESS | cut -d " " -f 26 | cut -d"/" -f 2)
    if [[ $curprefix -lt 1 || $curprefix -gt 32 ]]; then
        curprefix=""
    fi
    prefix=0
    while [[ $prefix -lt 1 || $prefix -gt 32 ]]; do
        echo -n "Prefix: [$curprefix]"
        read prefix
        if [[ $prefix = "" ]]; then
            prefix=$curprefix
        fi
    done
cat << EOF >> $file
PREFIX=$prefix
EOF
    onboot=""
    while [ "$onboot" != "yes" ] && [ "$onboot" != "no" ]; do
      echo -n "Onboot? [yes|no] "
      read onboot
    done
cat << EOF >> $file
ONBOOT=$onboot
EOF
     gatewayformatcorrect="false"
     curgateway=$(nmcli dev show eth0 | grep IP4.GATEWAY | cut -d " " -f 29)
     is_valid_ipv4 $curgateway

     if [[ $? -gt 0 ]]; then
       curgateway=""
     fi

     while [ "$gatewayformatcorrect" != "true" ]; do
        echo -n "Gateway? [$curgateway]"
        read gateway
        if [[ $gateway = "" ]]; then
          gateway=$curgateway
        fi
        is_valid_ipv4 $gateway
        if [[ $? -gt 0 ]]; then
            gatewayformatcorrect="false"
        else
            gatewayformatcorrect="true"
        fi
     done
cat << EOF >> $file
GATEWAY=$gateway
EOF

}
########################################
# main
########################################
if [ $EUID != 0 ]; then
        sudo "$0" "$@"
            exit $?
        fi
# first check if only 1 argument is given, otherwise print short help message
if [ $# -ne 1 ]; then
 echo "usage: nic-set-static NIC"
 exit 1
else
 nic=$1
 file="/etc/sysconfig/network-scripts/ifcfg-$nic"
 bufile="/etc/sysconfig/network-scripts/old.ifcfg-$nic"
fi
# next check if a config file exists for the given nic.
if [ ! -f $file ]; then
    createfile=""
    while [ "$createfile" != "yes" ] && [ "$createfile" != "no" ]; do
      echo -n "file does not exist. Create one? [yes|no] "
      read createfile
    done
      if [ $createfile == "yes" ]; then
        echo "answered yes"
        create_file
        echo "New config written to $file"
      elif [ $createfile == "no" ]; then
        echo "Exiting script"
        exit 0
      fi
else
    echo "A config file exists. It'll be renamed to old.ifcfg-$nic and a new config file will be created."
    mv $file $bufile
    if [[ $? -gt 0 ]]; then
        echo "There was an error in renaming the file. Please correct this manually and rerun this script."
        exit 1
    else
        create_file
    fi
fi
