# nic-set-static
Tool to quickly set the current DHCP address as a static address in RHEL 7 based on the current ip address configuration. If no configuration or new parameters.

Works only for IPV4 and creates a minimal config with following parameters:
DEVICE=
BOOTPROTO=
IPADDR=
PREFIX=
ONBOOT=
GATEWAY=

If a config for the given nic exists, it'll be moved to old.config.

